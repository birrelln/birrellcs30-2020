/*
Computer Science 30 Assignment One
Assorted things to solve.
Name:
Date:
 */

// change this to your package name (your last name) if the IDE doesn't do it for you
package assignments;

public class AssignmentOne {

    /*
    Ask the user for the temperature in Celsius, convert it to Fahrenheit, and display it to the user
     */
    public static void celsiusToFahrenheit(){

    }

    /*
    Asks the user for number of seconds.  Convert this to Days:Hours:Minutes:Seconds
    For example, 100000 seconds would be 1:3:46:40
     */
    public static void secondsToDays(){

    }

    /*
    Create two turtles.  Have the first draw a triangle and the second draw a hexagon.  The shapes should not overlap.
    Output how far away the two turtles are from each other at the end.
     */
    public static void turtlePower(){

    }

    /*
    Use indexOf and substring to create a new string with the target removed from the source.  Print the new string.
    Try to create a general solution that works for any source and target, provided that the target exists in the source.
     */
    public static void pizza(){
        String source = "Ham and pineapple is not a pizza.";
        String target = "not";
    }

    /*
    Prints a random integer from 20 - 80 inclusive.
     */
    public static void myRandom(){

    }

    public static void main(String[] args) {

        celsiusToFahrenheit();

        secondsToDays();

        turtlePower();

        pizza();

        myRandom();
        myRandom();
        myRandom();

    }
}
