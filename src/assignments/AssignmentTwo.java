/*
Computer Science 30 Assignment Two
Assorted things to solve.  Now with loops!
Name:
Date:
 */

package assignments;

public class AssignmentTwo {

    /*
    Use a for loop to print the even number from 2 to 100, inclusive.
     */
    public static void even(){

    }

    /*
    Ask the user how many integers they want to sum.  Use a while loop to ask for and sum that many numbers.  Keep track
    of the count of how many negative entries, positive entries, and entries equal to 0 that there were.  Use an
    if\else if\ else chain instead of separate if statements to do this.  Display the sum and the other stats at the
    end.

    Possible program run:

    How many integers do you want to sum?  3

    Enter integer: -1
    Enter integer: 0
    Enter integer: -10

    Sum: -11
    Number of zeroes: 1
    Number of negative integers: 2
    Number of positive integers: 0
     */
    public static void sumStats(){

    }

    /*
    Asks the user how many coin flips they wish to simulate.  Use a loop to simulate flipping a coin that many times.
    Output the total number flips, number of heads, percentages of heads, number of tails, and  percentage of tails.
     */
    public static void coinFlipper(){

    }

    public static void main(String[] args) {
        even();
        System.out.println();

        sumStats();
        System.out.println();

        coinFlipper();
    }
}
