package birrell;

/**
 * Class to simulate an animal
 */
public class Animal {
    private String species;
    private String name;
    private int limbs;
    private Animal parent;
    public static int animalCounter = 0;
    public static final int maxLimbs = 10;

    /*
    Default animal with 8 limbs
     */
    public Animal(){
        species = "thing";  // species of the animal
        name = "Bob";   // name of the animal
        limbs = 8;  // how many limbs the animal currently has
        animalCounter += 1;  // how many animals have been created
    }

    /**
     * Consructor that takes species, name, and number of limbs of animal
     * @param species species you want
     * @param name name of your animal
     * @param limbs how many limbs it has
     */
    public Animal(String species, String name, int limbs){
        this.species = species;
        this.name = name;
        setLimbs(limbs);

        animalCounter += 1;
    }

    public Animal(String species, String name, int limbs, Animal parent){
        this.species = species;
        this.name = name;
        this.limbs = limbs;
        this.parent = parent;

        animalCounter += 1;
    }

    /**
     *
     * @return name of animal
     */
    public String getName(){
        return name;
    }

    public String getSpecies(){
        return species;
    }

    /**
     *
     * @param name new animal name
     */
    public void setName(String name){
        this.name = name;
    }

    public Animal getParent(){
        return parent;
    }

    public void setLimbs(int limbs){

        if (limbs < 0){
            limbs = 0;
        }

        this.limbs = limbs;
    }

    public String toString(){
        String stats = name + " the " + species;
        return stats;
    }
}
