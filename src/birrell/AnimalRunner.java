package birrell;

public class AnimalRunner {
    public static void main(String[] args) {
        Animal myAnimal = new Animal();
        Animal testAnimal = new Animal("cat", "Joe", 4);
        Animal pet = new Animal("dog", "Billy", 3, testAnimal);
        myAnimal.setName("Jen");
        System.out.println(myAnimal.getName());

        System.out.println(testAnimal.getSpecies());

        System.out.println(Animal.animalCounter);

        System.out.println(testAnimal.getParent());

        System.out.println(pet);
        System.out.println(pet.getParent());
        Animal petParent = pet.getParent();
        System.out.println(petParent);
        petParent.setName("Wut");
        System.out.println(testAnimal);


    }
}
