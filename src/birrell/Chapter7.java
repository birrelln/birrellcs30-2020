package birrell;

public class Chapter7 {
    public static void main(String[] args) {
        int[] scores;

        scores = new int[5];

        String[] names = new String[10];

        int[] highScores = {299, 150, 34};

        scores[0] = 100;
        scores[4] = -56;

//        System.out.println(scores[0]);

        for (int i = 0; i < scores.length && i < 3; i++){
            System.out.print(scores[i] + " ");
        }

        System.out.println();
        System.out.println();

        for (int i = scores.length - 1; i >= 0; i--){
            System.out.print(scores[i] + " ");
        }
    }
}
