package birrell;

import java.util.ArrayList;

public class Chapter8 {


    public static void main(String[] args) {
        ArrayList<Integer> names = new ArrayList<Integer>();
        ArrayList<Player> players = new ArrayList<>();
        players.add(new Player());
        names.add(34);
        names.add(75);
        names.add(42);
        names.add(56);
        names.add(0, 99);
        names.remove(1);
        names.set(1, 101);
        System.out.println(names.get(0));
        System.out.println(names);
        System.out.println(names.size());
        System.out.println(players);

        Integer total = 0;
        for (Integer value: names){
            total += value;
        }
        System.out.println(total);

        total = 0;
        for (int i = 0; i < names.size(); i++){
            total += names.get(i);
        }
        System.out.println(total);

        total = 0;
        int i = 0;
        while (i < names.size()){
            total += names.get(i);
            i++;
        }
        System.out.println(total);
    }
}
