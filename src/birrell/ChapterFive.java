package birrell;

public class ChapterFive {

    public static void birthday(String name, int repeat){

        for(int i = 0; i < repeat; i++) {
            System.out.println("Happy Birthday dear " + name);
        }
    }

    /**
     * This method is used to have a dog bark.
     * @param none
     * @return none.
     */
    public static void speak(){
        System.out.println("Bark");
    }

    public static void eat(){
        System.out.println("I ate food");
    }

    /**
     * This method is used to multiplt an integer by itself.
     * If the number is 7, it messes everything up
     * @param number The integer you want to square
     * @return int The number squared.
     */
    public static int square(int number){

        if (number == 7){
            return 42;
        }
        return number * number;

    }

    public static void main(String[] args) {
//        birthday("Mr. Birrell", -7);
//        speak();
//        birthday("Caelan", 8);
        int number = 7;

        System.out.println(number);
        System.out.println(square(number));
        System.out.println(number);
    }
}

