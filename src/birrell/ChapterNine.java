package birrell;

public class ChapterNine {
    public static void main(String[] args) {
        int[][] myGrid;

        myGrid = new int[3][4];

        myGrid[0][2] = 42;

        System.out.println(myGrid[0][2]);
        System.out.println(myGrid.length);
        System.out.println(myGrid[0].length);

        System.out.println();

        for (int row = 0; row < myGrid.length; row++){
            for (int column = 0; column < myGrid[0].length; column++){
                System.out.print(myGrid[row][column] + " ");
            }
            System.out.println();
        }
    }
}
