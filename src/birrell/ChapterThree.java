package birrell;

public class ChapterThree {
    public static void main(String[] args) {
        boolean isHungry = false;
        boolean haveFood = false;

        int number = 3;

        if (isHungry && haveFood){
            System.out.println("You should eat");
        }
        else if (isHungry || haveFood){
            System.out.println("You could eat");
        }

        if (number < 0 || number > 10){
            System.out.println("Invalid selection");
        }
        else{
            System.out.println("Good to go");
        }

        boolean isDone = false;

        if (!isDone){
            System.out.println("Why not?");
        }
    }
}
