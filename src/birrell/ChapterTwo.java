package birrell;

public class ChapterTwo {
    public static void main(String[] args) {
        String greeting1 = "Hello!";
        String greeting2 = new String("Welcome!");
        String message = greeting1 + greeting2;

        System.out.println(message.indexOf("llp"));

        int score = 3;

        Integer highScore = 4;

        Double numbers = 4.4;

        System.out.println(Integer.MIN_VALUE);

        int randomNumber;

        // (int)(Math.random()*range) + min

        randomNumber = (int)(Math.random() * 10) + 10;

        System.out.println(randomNumber);

    }
}
