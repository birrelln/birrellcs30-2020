package birrell;

public class Player {

    private String name;  // name of player
    private int health;  // current health of player
    private int attackDamage;  // how much attack damage player does
    private int playerNumber; // order of creation

    private static final int maxDamage = 20;
    private static final int maxHealth = 1000;

    public static int playersCreated = 0;

    public Player(){
        playersCreated++;
        name = "Change Me";
        health = 100;
        attackDamage = 5;
        playerNumber = playersCreated;

    }

    public Player(String name, int health, int attackDamage){
        playersCreated++;
        this.name = name;
        this.health = health;
        this.attackDamage = attackDamage;
        playerNumber = playersCreated;
    }

    public void setName(String name){
        this.name = name;
    }

    public boolean setAttackDamage(int attackDamage){
        if (attackDamage < 0){
            return false;
        }

        if (attackDamage > maxDamage){
            attackDamage = maxDamage;
        }

        this.attackDamage = attackDamage;
        return true;
    }

    public void setHealth(int health){
        if (health < 0){
            health = 0;
        }

        this.health = health;
    }

    public String getName(){
        return this.name;
    }

    public int getPlayerNumber(){
        return playerNumber;
    }

    // prints out nicely formatted stats for player
    public void printStats(){
        System.out.println("Name: " + name + " Health: " + health + " Attack: " + attackDamage);
    }

    public String toString(){
        return name;
    }

    public static String convert(int thing){
        if (thing == maxDamage){
            return "Max Damage";
        }
        switch(thing){
            case maxDamage: {
                return "Max Damage";
            }
            case maxHealth:{
                return " Max Health";
            }

        }

        return("Could Not Convert");
    }

    public static void main(String[] args) {
        System.out.println("Hey");
        Player three = new Player();

        // since this main is inside the class, private means nothing
        three.name = "Yertle";
    }

}
