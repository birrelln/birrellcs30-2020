package birrell;

public class PlayerDemo {
    public static void main(String[] args) {

        System.out.println(Player.playersCreated);
        Player one = new Player();
        Player two = new Player("Bob", 200, 15);

        one.setName("Jill");

        System.out.println(one);
        System.out.println(two);

        System.out.println(Player.playersCreated);

        one.printStats();
        two.printStats();

    }

}
