package birrell;

import turtles.*;

public class TurtleTest
{
    public static void main(String[] args)
    {
        World world = new World(300,500);
        Turtle yertle = new Turtle(200, 300, world);
        Turtle leo = new Turtle(world);

        yertle.forward(50);
        yertle.turnLeft();
        yertle.forward(200);
        yertle.turnLeft();
        yertle.forward(10);

        leo.turnRight();
        leo.forward();

        System.out.println("Yertle width: " + yertle.getXPos());

        world.show(true);
    }
}