int x, y, xSpeed, diameter;

float number;

void setup(){
  size(1280, 720, P2D);
  x = width / 2;
  y = height / 2;
  xSpeed = 5;
  diameter = 80;
  number = 13.4;
}

void draw(){
  background(255);
  fill(255, 0, 0);
  ellipse(x, y, diameter, diameter);
  x += xSpeed;
  
  rect(mouseX, mouseY, 50, 50);
  
  if (x > width - diameter / 2){
    x = width - diameter / 2;
    xSpeed *= -1;
  }  
}

void keyPressed(){
  if (key == 'a'){
    xSpeed = -5;
  }
  else if (key == 'd'){
    xSpeed = 5;
  }
}

void keyReleased(){
  if (key == 'a' || key == 'd'){
    xSpeed = 0;
  }
}

void mouseClicked(){

}
