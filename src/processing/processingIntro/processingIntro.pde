int x, y;

void setup(){
  size(1280, 720);
  x = 5;
  y = 10;
}

void draw(){
  background(255, 128, 0);
  
  strokeWeight(5);
  line(100, 50, 600, 400);
  
  strokeWeight(1);
  fill(0, 255, 0);
  rect(100, 200, 150, 75);
  
  ellipse(600, 300, 200, 50);
  
  textSize(32);
  fill(0);
  text("What you want to say", 10, 50);
}
